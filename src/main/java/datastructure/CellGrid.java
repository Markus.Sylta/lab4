package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {
    int rows;
    int columns;
    cellular.CellState[][] CellState;

    public CellGrid(int rows, int columns, CellState initialState) {

        this.rows = rows;
        this.columns = columns;
        this.CellState = new CellState[rows][columns];
	}

    @Override
    public int numRows() {

        return this.rows;
    }

    @Override
    public int numColumns() {
        return this.columns;
    }

    @Override
    public void set(int row, int column, CellState element) {
        this.CellState[row][column] = element;
    }

    @Override
    public CellState get(int row, int column) {
        return this.CellState[row][column];
    }

    @Override
    public IGrid copy() {
        CellGrid copy = new CellGrid(this.rows, this.columns, null);
        for (int row = 0; row < this.rows; row++) {
            for (int col = 0; col < this.columns; col++) {
                copy.set(row, col, this.get(row, col));
            }
        }
        return copy;
    }
    
}
